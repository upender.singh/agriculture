<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'desertagriculture');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '`d$N<zj1JGnicvHOLz?KvW4WDk,S2K;auAfA3TShx4Qb][]uDDvuH==2kqCzboqB');
define('SECURE_AUTH_KEY',  'T0t1s`o-b.ejh*-!yZaa6ox@7^<lRChc_Jdb/1Htv$@QUdOK@1CX3V/JcJ[I{1+@');
define('LOGGED_IN_KEY',    '4fxm]7>b-4>47M!_aS>5^*Z}@twAI?.6ME/-B6yC;Z`=H!k,le-KI)%i;@O&^h9R');
define('NONCE_KEY',        ',#O5alF?}LOw><j$v@q+bKw}pl1}iJXTu8*v9%,y#pL}L%5|-!A{MAjCO!7*Mc[}');
define('AUTH_SALT',        '8gx0NQRMQaY*JVPyo-r4+b5yz[j{lx7t4#<~4=+RtXlOSTe@k1}(!}))%}]Ij?ud');
define('SECURE_AUTH_SALT', ':YahR5z^ra^oqU*~;HdN6<8vf4YKS:FA7e$,sPoU>N[*]ppRCHw]{Eg`Q| 9 {OW');
define('LOGGED_IN_SALT',   'zE>C-F]2[zIC,?_)!%X>Q4F?.Z=xgzXO^YX~fbc.s],MyRJ~-kk/0VWwat@@J?rI');
define('NONCE_SALT',       '}pYS?2/_$vEWH8s[Gi^~SPwmdcd>Fv|p e.ID9*5{ERQ@lSCeArx.%Vf4(SiopWJ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
