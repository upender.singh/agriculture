<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */
?>

<footer id="contact" style="background:url(<?php the_field('contact_background'); ?>) no-repeat;background-size:cover;">
    <div class="grid-container grid-x">
        <div class="small-12 medium-6 large-6">
            <div class="cotcstxtinner">
                <h2><?php the_field('contact_heading'); ?></h2>
                <?php the_field('contact_description'); ?>
            </div>
        </div>
        <div class="small-12 medium-6 large-6">
            <div class="frminner">
                <?php echo do_shortcode('[contact-form-7 id="104" title="Contact form 1"]'); ?>
            </div>
        </div>
        <div class="small-12 ad">
        <?php the_field('address'); ?>
        <ul class="socio">
            <?php
                // check if the repeater field has rows of data
                if( have_rows('social_icons') ):

                    // loop through the rows of data
                    while ( have_rows('social_icons') ) : the_row();
                ?>
                    <li><a href="<?php the_sub_field('link'); ?>" target="_blank"><img src="<?php the_sub_field('image'); ?>" class="img"></a></li>
                <?php
                    endwhile;

                else :

                    // no rows found

                endif;

            ?>
        </ul>
        <p class="copyrght"><?php the_field('copyright_text'); ?></p>
        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<script>
    $('document').ready(function(){
        var x = $('header').outerHeight();
        $('.banner').css('margin-top' , x);
        if($(window).width() < 767)
        {
            $('.responsiveicon').click(function(){
                $('.menu-main-menu-container').slideToggle();
            });
            $('.menu-main-menu-container li a').click(function(){
                $('.menu-main-menu-container').slideToggle();
            });
        }
    });
</script>
</div>
</body>
</html>