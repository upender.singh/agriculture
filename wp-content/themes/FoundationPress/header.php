<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<link rel="stylesheet" href="<?php bloginfo(template_url); ?>/style.css">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<div class="main-wrap">
		<header>
			<div class="grid-x align-justify align-middle">
				<div class="cell small-4 medium-3 large-4">
					<img src="<?php the_field('logo') ?>" class="img">
				</div>
				<div class="cell small-8 medium-9 large-8">
					<div class="responsiveicon">
						<img src="<?php bloginfo(template_url) ?>/src/assets/images/toggleicon.png" class="img">
					</div>
				 <?php wp_nav_menu('top-bar-r'); ?>
				</div>
			</div>
		</header>
	