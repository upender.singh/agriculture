<?php
/*
Template Name: Front
*/
get_header(); ?>

<section class="banner grid-x align-left align-middle" style="background:url(<?php the_field('banner_image'); ?>) right bottom no-repeat">
<div class="grid-container">
    <div class="cell small-6">
            <div class="bannertxt">
                <h1><?php the_field('banner_heading'); ?></h1>
                <h2><?php the_field('banner_sub_heading'); ?></h2>
                <?php the_field('banner_description'); ?>
                <a href="#grow-in-desert"><img src="<?php bloginfo(template_url) ?>/dist/assets/images/droparow.png"></a>
            </div>
    </div>  
</div>
</section>

<!-- grow in desert -->
<section class="grwindsrt grid-x align-middle" id="grow-in-desert" style="background:url(<?php the_field('grow_in_desert'); ?>)no-repeat">
    <div class="grid-container">
        <div class="small-12">
            <h2 class="heading"><?php the_field('grow_in_desert_heading'); ?></h2>
            <?php the_field('grow_in_desert_description'); ?>
        </div>
    </div>
</section>

<!-- the farmer burden -->
<section class="farmerch grid-x" id="farmer-challenge" style="background:url(<?php the_field('farmer_burden'); ?>) no-repeat">
    <div class="grid-container">
    <div class="small-12 farmerdet">
            <h2 class="heading"><?php the_field('farmer_burden_heading'); ?></h2>
            <?php the_field('farmer_burden_description'); ?>
        </div>
    </div>
</section>

<!-- the answer -->
<section class="answer" id="the-answer" style="background:url(<?php the_field('the_answer'); ?>) no-repeat">
    <div class="grid-container">
        <div class="small-12 farmerdet">
            <h2 class="heading"><?php the_field('the_answer_heading'); ?></h2>
            <?php the_field('the_answer_description'); ?>
        </div>
        <ul class="grid-x">
        <?php
                // check if the repeater field has rows of data
                if( have_rows('the_answer') ):

                    // loop through the rows of data
                    while ( have_rows('the_answer') ) : the_row();
                    ?>
                        <li>
                        <h3><?php the_sub_field('heading'); ?></h3>
                        <?php the_sub_field('description'); ?>
                        </li>
                <?php
                    endwhile;

                else :

                    // no rows found

                endif;

                ?>
            
        </ul>
    </div>
</section>

<!-- system -->
<section class="hydroponicsys" id="system">
    <div class="grid-container">
        <h2 class="heading">Our Hydroponic Systems</h2>
    </div>
    <div class="grid-x">
        <div class="small-12 medium-6 large-6 sysmain">
            <div class="innersystem">
                <img src="<?php the_field('hydrofarm'); ?>" class="img">
                <div class="det clr1">
                    <h3><?php the_field('hydrofarm_heading'); ?></h3>
                    <ul>
                    <?php

                        // check if the repeater field has rows of data
                        if( have_rows('hydrofarm_repeater') ):

                            // loop through the rows of data
                            while ( have_rows('hydrofarm_repeater') ) : the_row();
                        ?>
                         <li><?php  the_sub_field('list'); ?></li>
                        <?php
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>
                    </ul>
                </div>
            </div>
        </div>

        <div class="small-12 medium-6 large-6 sysmain sysmain2">
            <div class="innersystem">
                <img src="<?php the_field('green_houses'); ?>" class="img">
                <div class="det clr1">
                    <h3><?php the_field('greenhouses_heading'); ?></h3>
                    <ul>
                    <?php

                        // check if the repeater field has rows of data
                        if( have_rows('greenhouse_repeater') ):

                            // loop through the rows of data
                            while ( have_rows('greenhouse_repeater') ) : the_row();
                        ?>
                         <li><?php  the_sub_field('list'); ?></li>
                        <?php
                            endwhile;

                        else :

                            // no rows found

                        endif;

                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-container growmain">
        <h3><?php the_field('hydroponics_sub_heading') ?></h3>
        <div class="grid-x">
            <?php
            // check if the repeater field has rows of data
            if( have_rows('hydroponics_repeater') ):

                // loop through the rows of data
                while ( have_rows('hydroponics_repeater') ) : the_row();
            ?>
                <div class="small-12 medium-6 large-3 text-center">
                    <div class="grwinner">
                        <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                        <h4><?php the_sub_field('heading'); ?></h4>
                    </div>
                </div>
            <?php
                endwhile;

            else :

                // no rows found

            endif;

            ?>
        </div>
        <div class="grid-x">
            <h3 class="mrtp"><?php the_field('animal_fodder_heading'); ?></h3>
            <span class="it"><?php the_field('animal_fodder_sub_heading'); ?></span>
            <p><?php the_field('animal_fodder_description'); ?></p>
        </div>
        <div class="grid-x">
            <?php
            // check if the repeater field has rows of data
            if( have_rows('fodder_repeater') ):

                // loop through the rows of data
                while ( have_rows('fodder_repeater') ) : the_row();
            ?>
                <div class="small-12 medium-6 large-3 text-center">
                    <div class="grwinner">
                        <img src="<?php the_sub_field('image'); ?>" class="img-responsive">
                        <h4><?php the_sub_field('heading'); ?></h4>
                    </div>
                </div>
            <?php
                endwhile;

            else :

                // no rows found

            endif;

            ?>
        </div>
    </div>
</section>

<!-- services -->
<section class="services grid-x align-middle" id="services" style="background:url(<?php the_field('service_bg'); ?>) no-repeat">
    <div class="grid-container">
        <div class="small-12">   
            <h2><?php the_field('services_heading'); ?></h2>
            <ul>
                <?php
                // check if the repeater field has rows of data
                if( have_rows('services_repeater') ):

                    // loop through the rows of data
                    while ( have_rows('services_repeater') ) : the_row();
                ?>
                    <li>
                        <?php the_sub_field('text'); ?>
                    </li>
                <?php
                    endwhile;

                else :

                    // no rows found

                endif;

                ?>
            </ul>
        </div>
    </div>
</section>

<!-- about desert -->
<section class="abtagriculture grid-x align-middle" id="about" style="background:url(<?php the_field('about_background'); ?>) no-repeat">
    <div class="abtagricultureinner grid-x align-middle">
        <div class="small-12 medium-9 large-9">
            <h2><?php the_field('about_agriculture_heading'); ?></h2>
            <?php the_field('about_agriculture_description'); ?>
        </div>
        <div class="small-12 medium-3 large-3">
                <img src="<?php the_field('about_desert_agriculture_logo'); ?>" class="img">
        </div>
    </div>
</section>

<?php get_footer();
